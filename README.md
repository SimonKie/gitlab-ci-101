# Gitlab CI/CD 101

In this short presentation, you'll find a quick introduction to the basic terms and concepts of Gitlab CI/CD.

## Viewing the presentation

You can:

### View the plain markdown

Have a look at the plain markdown file: [presentation.md](./presentation.md)

Might look odd here and there.

### View the presentation

Run

```bash
make run
```

in the repository root to start a python server and navigate to localhost at the given port.
