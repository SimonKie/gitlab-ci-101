class: center, middle, inverse-blue

# Everything you need 
# to know  about Gitlab CI/CD*

*aka "What I know about Gitlab CI/CD"

<small>Simon Kienzler</small>

---

## Agenda

* Basics
* `.gitlab-ci.yaml`
* Runners
* Variables and Secrets
* Schedules

---

## Basics

* *Gitlab CI/CD* is the name of a tool for the Gitlab Suite, for doing
  * Continuous Integration (CI)
  * Continuous Delivery (CD)
  * Continuous Deployment (CD)
* It's pretty awesome (Fanboy alert)
* Find it in every Gitlab Project under "CI/CD" on the left

---

## The Workflow (not specific to GitLab)

![CI/CD Workflow](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)

---

## Your new favourite file:

## `.gitlab-ci.yaml`

```yaml
before_script:
  - apt-get install rubygems ruby-dev -y

run-test:
  script:
    - ruby --version
```

* 1 single file for managing your entire CI/CD strategy
* Usually located in the repo root
* define setup, steps, order, and more
* The docs are your friend to find out what you can do

---
class: center, middle

![Pipelines](https://docs.gitlab.com/ee/ci/img/pipelines.png)
An everyday Gitlab CI/CD pipeline.

---

## How it's configured

```yaml
stages:
- build
- test
- staging
- production

build-job:
  stage: build
  script: make build

test-frontend:
  stage: test
  script: make test frontend

test-backend:
  stage: test
  script: make test backend

hans-peter: # job names can be anything
  stage: staging
  script: make deploy staging

prod-deploy:
  stage: production
  script: make deploy production
  when: manual
```

---

## `.gitlab-ci.yaml`: other features

[Refer to the .gitlab-ci.yaml docs for detailed information](https://docs.gitlab.com/ee/ci/yaml/README.html)

* define a certain image to run the script with (alpine, ruby, php, etc.)
* use `only` and `except` to specify which git actions will trigger a job
* make `artifacts` available (to the Gitlab UI), and use them with `dependencies`
* use `extends` to template jobs, and `include` to split your config in several files
* define `tags` to specify which runner to use

Ah yes, runners! Smooth transition!

---

## Runners

Something's gotta execute those jobs!

> GitLab Runner is written in Go and can be run as a single binary, no language
> specific requirements are needed.

> It is designed to run on the GNU/Linux, macOS, and Windows operating systems.
> Other operating systems will probably work as long as you can compile a Go
> binary on them.

[Gitlab Runner Docs](https://docs.gitlab.com/runner/)

---

## Runner Executors

* Shell (kinda bare-metal, you need to install everything you need)
* VM (image must exist, runner clones and executes it)
* Docker (you can specify images to use and services to deploy additionally)
* Docker Machine (allows for auto-scaling)
* Kubernetes (YEAH! Connect to cluster and deploy job pods)
* SSH (ssh into server, execute there - not recommended)
* Custom (As if there weren't enough options already)

---

## How to get your runner ready

* Install
  * On a Debian based Machine
  * On Kubernetes
  * [etc.](https://docs.gitlab.com/runner/install/index.html)
* Register
  * To use a runner, it needs to be registered with your GitLab instance
  * `sudo gitlab-runner register` on the machine hosting the runner (Q/A)
  * While doing this, you can set the runners tags
  * [docs](https://docs.gitlab.com/runner/register/index.html)
* Use in the Project
  * more info on the next slide

---

## Using runners in a Project

There are three types of runners:

* Specific
  * means dedicated to the project
* Group
  * available fo all projects in the group
* Shared
  * shared between different projects, not related to groups

---

## Variables and Secrets

There are two types of variable concepts inside Gitlab CI/CD:

* Predefined and Custom Variables
* Secrets

Both are available as environment variables during runtime of the job

---

## Variables

**Predefined**: Available for every pipeline automatically, just use them

Examples:

* CI_COMMIT_SHA
* CI_MERGE_REQUEST_TITLE
* CI_PIPELINE_ID
* CI_PROJECT_NAME
* [etc.](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

**Custom**: Configure whatever you like in your `.gitlab-ci.yaml` or in the UI

```yaml
variables:
  TEST: "HELLO WORLD"
```

---

## Secrets

* Defined in the Settings of your Gitlab-Project
* Therefore only visible to Maintainers
* Can contain passwords, tokens, and other sensitive data the job needs

---

## Using variables and Secrets

Just use them.

```yaml
before_script:
  - echo $(REGISTRY_TOKEN) > /root/docker/.config

my-job:
  script: kaniko --destination=$(REGISTRY_ADDR)/$(APP_NAME):$(CI_COMMIT_SHA)

variables:
  APP_NAME: demo-app
  REGISTRY_ADDR: index.docker.io/simonkienzler

```

(Caution: Non-working example, shortened for brevity)

---

## Schedules

You can use schedules to execute pipelines based on time, not activity

![Schedules](./schedules.png)

---

## A simple real-life example

I use Gitlab CI/CD for deploying static files via FTP.

---

## That's about it

* Define your jobs, stages and pipelines with `.gitlab-ci.yaml`
* Install and use runners to execute them OR
* Use shared runners that someone else provides for you
* Reference secrets, predefined and custom variables as ENV Vars
* Make use of schedules for nightly builds etc.

---

## Not covered

* [Container Registries](https://docs.gitlab.com/ee/user/project/container_registry.html)
* [Auto DevOps](https://gitlab.com/help/topics/autodevops/quick_start_guide.md)
* [Environments](https://docs.gitlab.com/ee/ci/environments.html)
* &lt;disclaimer&gt;A lot of stuff I completely forgot about / do not know of&lt;/disclaimer&gt;

---

class: center, middle

# Questions?